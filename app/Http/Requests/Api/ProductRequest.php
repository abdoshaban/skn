<?php

namespace App\Http\Requests\Api;


class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'name' => 'required|alpha|min:4',
            'price' => 'required|numeric',
            'phone' => 'required|starts_with:0|min:11|max:11',

            'start_at' => 'required|date',
            'end_at' => 'required|date|after:start_at',

            'images' => 'required|array|max:4',
            'images.*' => 'mimes:png,jpeg,jpg'
        ];
    }
}
