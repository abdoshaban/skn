<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {

        if (!auth()->attempt(['username' => request('username'), 'password' => request('password')]))
            return response()->json(['error' => 'Unauthorised'], 401);

        return response()->json(['message' => 'you are login now']);
    }

}
