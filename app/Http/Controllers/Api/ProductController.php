<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Product;

class ProductController extends Controller
{


    public function store(ProductRequest $request)
    {
        $product = Product::create($request->all());
        foreach ($request->images as $image) {
            $product->addMedia($image)->toMediaCollection('images');
        }
        return response()->json(['message'=>'product added successfully']);
    }

    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json(['message'=>'product deleted successfully']);
    }
}
