<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Product extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $with = ['user'];

    protected $appends = [
        'images',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'start_at',
        'end_at',
    ];


    protected $fillable = [
        'name',
        'price',
        'phone',
        'user_id',
        'created_at',
        'updated_at',
        'start_at',
        'end_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->select(['id', 'username']);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }
    public function getImagesAttribute()
    {
        $files = $this->getMedia('images');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
        });

        return $files->pluck('url');
    }
}
